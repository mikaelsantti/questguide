﻿using QuestGuide.Dependencies;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuestGuide.Storage
{
    public class Settings
    {
        #region KEYS
       
        private const string serverUrl = "server";
        private const string dbName = "dbName";
        private const string user = "user";
        private const string password = "password";
        private const string itemCollection = "itemCollection";
        private const string questCollection = "questColletion";
        private const string dungeonCollection = "dungeonCollection";

        
        #endregion /KEYS

        private static string GetSetting(string key)
        {
            return NativeSettings.GetValue(key);
        }

        private static void SaveSetting(string key, string value)
        {
            NativeSettings.StoreValue(key, value);
        }
        public static string GetServerBaseUrl()
        {
            return GetSetting(serverUrl);
        }

        public static void SaveServerUrl(string value)
        {
            SaveSetting(serverUrl, value);
        }

        public static string GetDatabaseName()
        {
            return GetSetting(dbName);
        }

        public static void SaveDatabaseName(string value)
        {
            SaveSetting(dbName, value);
        }
        public static string GetUserName()
        {
            return GetSetting(user);
        }

        public static void SaveUserName(string value)
        {
            SaveSetting(user, value);
        }
        public static string GetPassword()
        {
            return GetSetting(password);
        }

        public static void SavePassword(string value)
        {
            SaveSetting(password, value);
        }
        public static string GetItemCollection()
        {
            return GetSetting(itemCollection);
        }

        public static void SaveItemCollection(string value)
        {
            SaveSetting(itemCollection, value);
        }
        public static string GetQuestCollection()
        {
            return GetSetting(questCollection);
        }

        public static void SaveQuestCollection(string value)
        {
            SaveSetting(questCollection, value);
        }
        public static string GetDungeonCollection()
        {
            return GetSetting(dungeonCollection);
        }

        public static void SaveDungeonCollection(string value)
        {
            SaveSetting(dungeonCollection, value);
        }
    }
}
