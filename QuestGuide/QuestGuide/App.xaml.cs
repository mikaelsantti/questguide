﻿using QuestGuide.Infrastructure;
using QuestGuide.Services;
using QuestGuide.Storage;
using QuestGuide.ViewModels;
using QuestGuide.Views;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace QuestGuide
{
    public partial class App : Application
    {
        private MongoService mongoService;

        public App()
        {
            InitializeComponent();

            mongoService = new MongoService();
            ServiceLocator.container.Register(mongoService);

            SettingsService.Configure();
            //MainPage = new NavigationPage(new MainPage());
            //MainPage = new AppShell();
        }
        private Task InitializeNavigation()
        {
            var navigationService = ViewModelLocator.Resolve<INavigationService>();
            var basev = navigationService.InitializeAsync();
            return basev;
        }

        protected override async void OnStart()
        {
            base.OnStart();
            try
            {
                await InitializeNavigation();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            base.OnResume();
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
