﻿using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace QuestGuide
{
    class TooltipHelper
    {
        public string Name { get; set; }
        public string Binds { get; set; }
        public string Slot { get; set; }
        public string Type { get; set; }
        public int MyProperty { get; set; }

        public static string Format(string tooltip)
        {
            var text = Regex.Replace(tooltip, "<[^>]*>", ";");
            string[] words = text.Split(';');
            var itemStats = "";
            foreach (var itm in words)
            {
                if (!string.IsNullOrWhiteSpace(itm))
                {
                    itemStats += itm + "\n";
                }
            }
            return itemStats;
        }

        public static List<string> FormatFirstPart(string tooltip)
        {
            var text = Regex.Replace(tooltip, "<[^>]*>", ";");
            string[] words = text.Split(';');

            List<string> temp = new List<string>();
            List<string> stats = new List<string>();
            for (int i = 0; i < 23; i++)
            {
                if (!string.IsNullOrWhiteSpace(words[i]))
                {
                    temp.Add(words[i]);
                }
            }
            if (temp.Count == 6)
            {
                stats.Add(temp[0]);
                stats.Add(temp[3]);
            }
            return stats;
        }

        public static List<string> FormatLastPart(string tooltip)
        {
            var text = Regex.Replace(tooltip, "<[^>]*>", ";");
            string[] words = text.Split(';');

            List<string> stats = new List<string>();
            for (int i = 24; i < words.Length; i++)
            {
                if (!string.IsNullOrWhiteSpace(words[i]))
                {
                    stats.Add(words[i]);
                }
            }
            return stats;
        }

    }
}
