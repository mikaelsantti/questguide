﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace QuestGuide.Dependencies
{
    public class NativeSettings
    {

        public static string GetValue(string key)
        {
            return DependencyService.Get<INativeSettings>().GetValue(key);
        }

        public static void StoreValue(string key, string value)
        {
            DependencyService.Get<INativeSettings>().StoreValue(key, value);
        }

        public static void RemoveValue(string key)
        {
            DependencyService.Get<INativeSettings>().RemoveValue(key);
        }
    }

    public interface INativeSettings
    {
        void StoreValue(string key, string value);
        string GetValue(string key);
        void RemoveValue(string key);
    }

}
