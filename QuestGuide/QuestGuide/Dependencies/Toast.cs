﻿using Xamarin.Forms;

namespace QuestGuide.Dependencies
{
    public class Toast
    {
        public static void Show(string message)
        {
            Device.BeginInvokeOnMainThread(() => { DependencyService.Get<IToast>().Show(message); });
        }
    }

    public interface IToast
    {
        void Show(string message);
    }
}
