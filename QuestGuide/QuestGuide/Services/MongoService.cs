﻿using MongoDB.Bson;
using MongoDB.Driver;
using QuestGuide.Models;
using QuestGuide.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;

namespace QuestGuide.Services
{
    public class MongoService
    {
        static IMongoCollection<Item> itemsCollection;
        static IMongoCollection<Quest> questCollection;
        static IMongoCollection<Dungeon> dungeonCollection;
        readonly static string dbName = "juhlamokka";
        readonly static string ItemCollection = Settings.GetItemCollection();
        readonly static string QuestCollection = Settings.GetQuestCollection();
        readonly static string DungeonCollection = Settings.GetDungeonCollection();
        static MongoClient client;

        public MongoService()
        {

        }
        public IMongoCollection<Item> ItemsCollection
        {
            get
            {
                if (client == null || itemsCollection == null)
                {
                    var url = Settings.GetServerBaseUrl();
                    MongoClientSettings settings = MongoClientSettings.FromUrl(
                        new MongoUrl(url));
                    settings.SslSettings = new SslSettings
                    {
                        EnabledSslProtocols = SslProtocols.Tls12
                    };

                    client = new MongoClient(settings);
                    var db = client.GetDatabase(dbName);

                    var collectionSettings = new MongoCollectionSettings
                    {
                        ReadPreference = ReadPreference.Nearest
                    };
                    itemsCollection = db.GetCollection<Item>(ItemCollection, collectionSettings);
                }
                return itemsCollection;
            }
        }
        public IMongoCollection<Quest> QuestsCollection
        {
            get
            {
                if (client == null || questCollection == null)
                {
                    var url = Settings.GetServerBaseUrl();
                    MongoClientSettings settings = MongoClientSettings.FromUrl(
                        new MongoUrl(url));
                    settings.SslSettings = new SslSettings
                    {
                        EnabledSslProtocols = SslProtocols.Tls12
                    };

                    client = new MongoClient(settings);
                    var db = client.GetDatabase(dbName);

                    var collectionSettings = new MongoCollectionSettings
                    {
                        ReadPreference = ReadPreference.Nearest
                    };
                    questCollection = db.GetCollection<Quest>(QuestCollection, collectionSettings);
                }
                return questCollection;
            }
        }
        public IMongoCollection<Dungeon> DungeonsCollection
        {
            get
            {
                if (client == null || dungeonCollection == null)
                {
                    var url = Settings.GetServerBaseUrl();
                    MongoClientSettings settings = MongoClientSettings.FromUrl(
                        new MongoUrl(url));
                    settings.SslSettings = new SslSettings
                    {
                        EnabledSslProtocols = SslProtocols.Tls12
                    };

                    client = new MongoClient(settings);
                    var db = client.GetDatabase(dbName);

                    var collectionSettings = new MongoCollectionSettings
                    {
                        ReadPreference = ReadPreference.Nearest
                    };
                    dungeonCollection = db.GetCollection<Dungeon>(DungeonCollection, collectionSettings);
                }
                return dungeonCollection;
            }
        }
    }
}
