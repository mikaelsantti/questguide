using QuestGuide.Storage;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuestGuide.Services
{
    static class SettingsService
    {
        public static void Configure()
        {
            MLabConfig();
        }
        private static void MLabConfig()
        {
            Settings.SaveDatabaseName("xxxxx");
            Settings.SaveUserName("xxxxx");
            Settings.SavePassword("xxxxx");
            Settings.SaveItemCollection("Items");
            Settings.SaveQuestCollection("Quests");
            Settings.SaveDungeonCollection("Dungeons");

            var username = Settings.GetUserName();
            var password = Settings.GetPassword();
            var dbName = Settings.GetDatabaseName();

            Settings.SaveServerUrl(
               "mongodb://" + username + ":" + password + "@ds235053.mlab.com:35053/" + dbName + "");
        }
    }
}
