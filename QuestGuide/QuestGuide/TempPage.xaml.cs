﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace QuestGuide
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TempPage : ContentPage
    {
        public TempPage()
        {
            InitializeComponent();
        }

        //private void GetItem(int id)
        //{
        //    var item = Item.GetItem(id);
        //    ItemName.Text = item.Name;
        //    if (item.Quality == 2)
        //    {
        //        ItemName.TextColor = Color.LimeGreen;
        //    }
        //    var itemStats = TooltipHelper.Format(item);

        //    ItemLevel.Text = itemStats[1] + ": " + itemStats[2];
        //    ItemBinds.Text = itemStats[3];
        //    ItemArmorType.Text = itemStats[4];
        //    ItemSlot.Text = itemStats[5];
        //    ItemArmorValue.Text = itemStats[6];
        //    ItemStats.Text = itemStats[7];
        //    ItemDurability.Text = itemStats[8];
        //    ItemSelPrice.Text = itemStats[9] + " " + itemStats[10] + "." + itemStats[11];
        //    ItemImage.Source = ImageSource.FromUri(new Uri("https://wow.zamimg.com/images/wow/icons/large/" + item.Icon + ".jpg"));
        //}

        private string GetPlainTextFromHtml(string htmlString)
        {
            string htmlTagPattern = "<.*?>";
            var regexCss = new Regex("(\\<script(.+?)\\)|(\\<style(.+?)\\)))", RegexOptions.Singleline | RegexOptions.IgnoreCase);
            htmlString = regexCss.Replace(htmlString, string.Empty);
            htmlString = Regex.Replace(htmlString, htmlTagPattern, string.Empty);
            htmlString = Regex.Replace(htmlString, @"^\s+$[\r\n]*", "", RegexOptions.Multiline);
            //htmlString = htmlString.Replace(" ", string.Empty);
            htmlString = htmlString.Replace(" ", ";");

            return htmlString;
        }
    }
}