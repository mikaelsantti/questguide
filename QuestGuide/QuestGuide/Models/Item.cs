﻿using Newtonsoft.Json;
using QuestGuide.Services;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace QuestGuide.Models
{
    public class Item
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quality { get; set; }
        public string Icon { get; set; }
        public string Tooltip { get; set; }
        public int QuestId { get; set; }
        public string ItemColor { get; set; }

        public Item()
        {
        }

        public Item(string name, int quality, string icon, string tooltip)
        {
            Name = name;
            Quality = quality;
            Icon = icon;
            Tooltip = tooltip;
        }

        public static ObservableCollection<Item> GetRewardList(List<int> rewardIDs, int questId)
        {
            ObservableCollection<Item> QuestRewardList;
            QuestRewardList = new ObservableCollection<Item>();
            foreach (var i in rewardIDs)
            {
                var item = GetItem(i);
                item.QuestId = questId;
                QuestRewardList.Add(item);
            }
            return QuestRewardList;
        }

        public static Item GetItem(int id)
        {
            var client = new RestClient("https://classic.wowhead.com/tooltip/item/" + id + "&json&power");
            var request = new RestRequest(Method.GET);
            var response = client.Execute(request);
            var i = JsonConvert.DeserializeObject<Item>(response.Content);
            i.Id = id;
            return i;
        }
        public static Item GetItem(int id, int questId)
        {
            var client = new RestClient("https://classic.wowhead.com/tooltip/item/" + id + "&json&power");
            var request = new RestRequest(Method.GET);
            var response = client.Execute(request);
            var i = JsonConvert.DeserializeObject<Item>(response.Content);
            i.Id = id;
            i.QuestId = questId;
            i.ItemColor = ItemRarityChecker(i);
            return i;
        }

        public static string ItemRarityChecker(Item item)
        {
            switch (item.Quality)
            {
                case 2:
                    return "#00d510";
                case 3:
                    return "#0070dd";
                default:
                    return "#ffffff";
            }
        }

        //public static ObservableCollection<Item> ItemData()
        //{
        //    var listOfItems = new ObservableCollection<Item>
        //    {
        //        //RFC quest rewards
        //        new Item { Id = 15452, QuestId = 5722 },
        //        new Item { Id = 15453, QuestId = 5722 },
        //        new Item { Id = 15449, QuestId = 5725 },
        //        new Item { Id = 15450, QuestId = 5725 },
        //        new Item { Id = 15451, QuestId = 5725 },
        //        new Item { Id = 15443, QuestId = 5728 },
        //        new Item { Id = 15445, QuestId = 5728 },
        //        new Item { Id = 15424, QuestId = 5728 },
        //        new Item { Id = 15444, QuestId = 5728 },
               

        //        // WC quest rewards
        //         new Item { Id = 6505, QuestId = 914 },
        //         new Item { Id = 6504, QuestId = 914 },
        //         new Item { Id = 6480, QuestId = 1486 },
        //         new Item { Id = 918, QuestId = 1486 },
        //         new Item { Id = 6476, QuestId = 1487 },
        //         new Item { Id = 8071, QuestId = 1487 },
        //         new Item { Id = 6481, QuestId = 1487 },

        //         // SFK quest rewards
        //         new Item { Id = 6335, QuestId = 1013 },
        //         new Item { Id = 4534, QuestId = 1013 },
        //         new Item { Id = 3324, QuestId = 1098 },
        //         new Item { Id = 6414, QuestId = 1014 },

        //         // BFD quest rewards
        //         new Item { Id = 17694, QuestId = 6565 },
        //         new Item { Id = 17695, QuestId = 6565 },
        //         new Item { Id = 7001, QuestId = 6561 },
        //         new Item { Id = 7002, QuestId = 6561 },

        //         //Gnomeregan quest rewards
        //         new Item { Id = 9623, QuestId = 2841 },
        //         new Item { Id = 9624, QuestId = 2841 },
        //         new Item { Id = 9625, QuestId = 2841 },
        //         new Item { Id = 9535, QuestId = 2904 },
        //         new Item { Id = 9536, QuestId = 2904 },
        //         new Item { Id = 9363, QuestId = 2951 },

        //         //RFK quest rewards
        //         new Item { Id = 4197, QuestId = 1102 },
        //         new Item { Id = 6742, QuestId = 1102 },
        //         new Item { Id = 6725, QuestId = 1102 },
        //         new Item { Id = 6755, QuestId = 1221 },

        //         new Item { Id = 6748, QuestId = 1144 },
        //         new Item { Id = 6750, QuestId = 1144 },
        //         new Item { Id = 6749, QuestId = 1144 }
        //    };
        //    return listOfItems;
        //}
    }
}
