﻿using QuestGuide.Services;
using QuestGuide.ViewModels;
using System;
using System.Globalization;
using System.Reflection;
using TinyIoC;
using Xamarin.Forms;

namespace QuestGuide.ViewModels
{
    public static class ViewModelLocator
    {
        private static readonly TinyIoCContainer container;

        public static readonly BindableProperty AutoWireViewModelProperty =
            BindableProperty.CreateAttached("AutoWireViewModel", typeof(bool), typeof(ViewModelLocator), default(bool), propertyChanged: OnAutoWireViewModelChanged);

        static ViewModelLocator()
        {
            container = new TinyIoCContainer();

            // VIEWMODELS
            container.Register<DungeonViewModel>();
            container.Register<ManagementViewModel>();

            // SERVICES
            container.Register<INavigationService, NavigationService>();

        }

        public static T Resolve<T>() where T : class => container.Resolve<T>();

        public static bool GetAutoWireViewModel(BindableObject bindable)
        {
            return (bool)bindable.GetValue(AutoWireViewModelProperty);
        }

        public static void SetAutoWireViewModel(BindableObject bindable, bool value)
        {
            bindable.SetValue(AutoWireViewModelProperty, value);
        }

        private static void OnAutoWireViewModelChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var view = bindable as Element;
            if (view == null)
            {
                return;
            }

            var viewType = view.GetType();
            var viewName = viewType.FullName.Replace(".Views.", ".ViewModels.");
            var viewAssemblyName = viewType.GetTypeInfo().Assembly.FullName;
            var viewModelName = string.Format(CultureInfo.InvariantCulture, "{0}Model, {1}", viewName, viewAssemblyName);

            var viewModelType = Type.GetType(viewModelName);
            if (viewModelType == null)
            {
                return;
            }
            var viewModel = container.Resolve(viewModelType);
            view.BindingContext = viewModel;
        }

        private static void AutomagicalTyperegistartion()
        {
            foreach (var type in Assembly.GetExecutingAssembly().GetTypes())
            {
                //Register ViewModels
                if (type.IsSubclassOf(typeof(BaseViewModel)))
                    container.Register(type);

                //Register Services
                if (typeof(IService).IsAssignableFrom(type))
                    container.Register(type);
            }

        }
    }
    internal interface IService { }
}
