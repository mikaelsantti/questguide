﻿using System;
using System.Collections.Generic;
using System.Text;
using TinyIoC;

namespace QuestGuide.Infrastructure
{
    public static class ServiceLocator
    {
        public static readonly TinyIoCContainer container;

        static ServiceLocator()
        {

            container = new TinyIoCContainer();
            // SERVICES

        }
        public static T Resolve<T>() where T : class => container.Resolve<T>();
    }
}
