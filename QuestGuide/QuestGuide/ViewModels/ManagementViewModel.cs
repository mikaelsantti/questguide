﻿using QuestGuide.Dependencies;
using QuestGuide.Models;
using QuestGuide.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace QuestGuide.ViewModels
{
    public class ManagementViewModel : BaseViewModel
    {
        
        private DungeonService dungeonService;
        private QuestService questService;
        private ItemService itemService;
        private ObservableCollection<Dungeon> dungeons;
        private ObservableCollection<Quest> quests;

        public ICommand InsertDungeonCommand { get; set; }
        public ICommand InsertQuestCommand { get; set; }
        public ICommand InsertItemCommand { get; set; }
        public ICommand UpdateDungeonCommand { get; set; }
        public ICommand UpdateQuestCommand { get; set; }

        public ObservableCollection<Dungeon> Dungeons
        {
            get { return dungeons; }
            set { dungeons = value; }
        }
        public ObservableCollection<Quest> Quests
        {
            get { return quests; }
            set { quests = value; }
        }
        private string updateQuestCoords { get; set; }
        public string UpdateQuestCoords
        {
            get
            {
                return updateQuestCoords;
            }
            set
            {
                updateQuestCoords = value;
                OnPropertyChanged("UpdateQuestCoords");
            }
        }

        private int dungeonId { get; set; }
        public int DungeonId
        {
            get
            {
                return dungeonId;
            }
            set
            {
                dungeonId = value;
                OnPropertyChanged("DungeonId");
            }
        }
        private int updateDungeonId;
        public int UpdateDungeonId
        {
            get
            {
                return updateDungeonId;
            }
            set
            {
                updateDungeonId = value;
                OnPropertyChanged("UpdateDungeonId");
            }
        }

        private string dungeonName;
        public string DungeonName
        {
            get
            {
                return dungeonName;
            }
            set
            {
                dungeonName = value;
                OnPropertyChanged("DungeonName");
            }
        }

        private string updateDungeonName;
        public string UpdateDungeonName
        {
            get
            {
                return updateDungeonName;
            }
            set
            {
                updateDungeonName = value;
                OnPropertyChanged("UpdateDungeonName");
            }
        }

        private int questId;
        public int QuestId
        {
            get
            {
                return questId;
            }
            set
            {
                questId = value;
                OnPropertyChanged("QuestId");
            }
        }

        private int questDungeonId;
        public int QuestDungeonId
        {
            get
            {
                return questDungeonId;
            }
            set
            {
                questDungeonId = value;
                OnPropertyChanged("QuestDungeonId");
            }
        }
        private int itemId;
        public int ItemId
        {
            get
            {
                return itemId;
            }
            set
            {
                itemId = value;
                OnPropertyChanged("ItemId");
            }
        }
        private int itemQuestId;
        public int ItemQuestId
        {
            get
            {
                return itemQuestId;
            }
            set
            {
                itemQuestId = value;
                OnPropertyChanged("ItemQuestId");
            }
        }
        public override Task InitializeAsync(object navigationData)
        {
            
            return base.InitializeAsync(navigationData);
        }
        //public ManagementViewModel(DungeonService dungeonService, QuestService questService, ItemService itemService, ObservableCollection<Dungeon> dungeonCollection)
        //{
        //    this.dungeonService = dungeonService;
        //    this.questService = questService;
        //    this.itemService = itemService;

        //    InsertDungeonCommand = new Command(() => InsertNewDungeon());
        //    InsertQuestCommand = new Command(() => InsertNewQuest());
        //    InsertItemCommand = new Command(async () => await InsertNewItemAsync());
        //    UpdateDungeonCommand = new Command(async () => await UpdateDungeonAsync());
        //    UpdateQuestCommand = new Command(async () => await UpdateQuestAsync());

        //    var dungeonCount = dungeonService.GetAllDungeons().Count();
        //    DungeonId = dungeonCount + 1;

        //    dungeons = dungeonCollection;
        //    Task.Run(async () => await PopulateQuestPicker());
        //}
        private async Task PopulateQuestPicker()
        {
            quests = new ObservableCollection<Quest>();
            var questList = await questService.GetAllQuestsAsync();
            foreach (var quest in questList)
            {
                quests.Add(quest);
            }
        }

        private async Task UpdateQuestAsync()
        {
            var quest = selectedQuest;
            quest.Coords = updateQuestCoords;
            await questService.UpdateQuest(quest);
        }

        private async Task UpdateDungeonAsync()
        {
            var dungeon = selectedDungeon;
            dungeon.Name = updateDungeonName;
            await dungeonService.UpdateDungeonAsync(dungeon);
        }

        private void InsertNewDungeon()
        {
            
            dungeonService.InsertDungeon(new Dungeon { Id = DungeonId, Name =  dungeonName});
        }
        private void InsertNewQuest()
        {
            var quest = Quest.GetQuests(QuestId, questDungeonId);
            questService.InsertQuest(quest);
        }
        private async Task InsertNewItemAsync()
        {
            var item = Item.GetItem(itemId, itemQuestId);
            await itemService.InsertItemAsync(item);
        }


        private Dungeon selectedDungeon;
        public Dungeon SelectedDungeon
        {
            get { return selectedDungeon; }
            set
            {
                try
                {
                    selectedDungeon = value;
                    UpdateDungeonId = selectedDungeon.Id;
                    UpdateDungeonName = selectedDungeon.Name;
                    Toast.Show("Selected dungeon: " + selectedDungeon.Id + " - " + selectedDungeon.Name);
                }
                catch (Exception ex)
                {
                    Toast.Show(ex.Message);
                }
            }
        }

        private Quest selectedQuest;
        public Quest SelectedQuest
        {
            get { return selectedQuest; }
            set
            {
                try
                {
                    selectedQuest = value;
                    UpdateQuestCoords = selectedQuest.Coords;
                    Toast.Show("Selected dungeon: " + selectedDungeon.Id + " - " + selectedDungeon.Name);
                }
                catch (Exception ex)
                {
                    Toast.Show(ex.Message);
                }
            }
        }
    }
}
