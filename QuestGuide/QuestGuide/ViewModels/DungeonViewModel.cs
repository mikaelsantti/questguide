﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using QuestGuide.Models;
using RestSharp;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using QuestGuide.Dependencies;
using System.Runtime.CompilerServices;
using static System.Net.Mime.MediaTypeNames;
using Xamarin.Forms;
using HtmlAgilityPack;
using System.Net;
using System.Web;
using MongoDB.Driver;
using QuestGuide.Services;
using System.Threading.Tasks;
using System.Windows.Input;
using QuestGuide.Views;
using System.Diagnostics;
using QuestGuide.Infrastructure;

namespace QuestGuide.ViewModels
{
    class DungeonViewModel : BaseViewModel
    {
        private INavigationService navigationService;
        private MongoService mongoService = ServiceLocator.Resolve<MongoService>();
        private ItemService itemService;
        private QuestService questService;
        private DungeonService dungeonService;
        private ImageSource itemIcon;
        private Dungeon selectedDungeon;
        private Quest selectedQuest;
        private Item selectedReward;

       

        private ObservableCollection<Dungeon> dungeons;
        private ObservableCollection<Quest> quests;
        private ObservableCollection<Item> rewards;
        
        private string itemBorders;
        private string coords;
        private string tapText;
        private string levelReq;
        private string additionalInfo;
        private string tooltip;
        private string rewardTooltip;
        public string Coords
        {
            get
            {
                return coords;
            }
            set
            {
                SetProperty(ref coords, value);
                //coords = value;
                //OnPropertyChanged("Coords");
            }
        }
        public string TapText
        {
            get
            {
                return tapText;
            }
            set
            {
                SetProperty(ref tapText, value);
                //tapText = value;
                //OnPropertyChanged("TapText");
            }
        }
        public string LevelReq
        {
            get { return levelReq; }
            set
            {
                SetProperty(ref levelReq, value);
                //levelReq = value;
                //OnPropertyChanged("LevelReq");
            }
        }
        public string AdditionalInfo
        {
            get { return additionalInfo; }
            set
            {
                SetProperty(ref additionalInfo, value);
                //additionalInfo = value;
                //OnPropertyChanged("AdditionalInfo");
            }
        }
        public string Tooltip
        {
            get
            {
                return tooltip;
            }
            set
            {
                SetProperty(ref tooltip, value);
                //tooltip = value;
                //OnPropertyChanged(nameof(Tooltip));
            }
        }
        public string RewardTooltip
        {
            get
            {
                return rewardTooltip;
            }
            set
            {
                rewardTooltip = value;
                OnPropertyChanged(nameof(RewardTooltip));
            }
        }
        public string ItemBorders
        {
            get { return itemBorders; }
            set
            {
                itemBorders = value;
                OnPropertyChanged("ItemBorders");
            }
        }
        public ICommand ManagementCommand => new Command(async () => await Test());
        public ICommand TooltipInfoCommand => new Command(async () => await Test());
        public ImageSource ItemIcon
        {
            get
            {
                return itemIcon;
            }
            set
            {
                SetProperty(ref itemIcon, value);
                //itemIcon = value;
                //OnPropertyChanged("ItemIcon");
            }
        }
        public ObservableCollection<Dungeon> Dungeons
        {
            get { return dungeons; }
            set { SetProperty(ref dungeons, value); }
        }
        public ObservableCollection<Quest> Quests
        {
            get { return quests; }
            set { SetProperty(ref quests, value); }
        }
        public ObservableCollection<Item> Rewards
        {
            get { return rewards; }
            set { SetProperty(ref rewards, value); }
        }
        public override Task InitializeAsync(object navigationData)
        {
            navigationService = ViewModelLocator.Resolve<INavigationService>();

            Dungeons = new ObservableCollection<Dungeon>();
            Quests = new ObservableCollection<Quest>();
            Rewards = new ObservableCollection<Item>();

            InitiateServices();
            PopulateDungeonPicker();

            return base.InitializeAsync(navigationData);
        }

        private async Task Test()
        {
            await navigationService.NavigateToAsync<ManagementViewModel>();
        }

        public void ShowAdditionalInfo()
        {
            var npc = selectedQuest.Npc;
            var coords = selectedQuest.Coords;
            var title = $"{npc} {coords}";
            var info = selectedQuest.AdditionalInfo;
            App.Current.MainPage.DisplayAlert(title, info, "Ok");
        }

        private void InitiateServices()
        {

            var itemsCollection = mongoService.ItemsCollection;
            var questCollection = mongoService.QuestsCollection;
            var dungeonCollection = mongoService.DungeonsCollection;

            itemService = new ItemService(itemsCollection);
            questService = new QuestService(questCollection);
            dungeonService = new DungeonService(dungeonCollection);

        }
        private void PopulateDungeonPicker()
        {
            var dungeonList = dungeonService.GetAllDungeons();
            foreach (var dungeon in dungeonList)
            {
                dungeons.Add(dungeon);
            }
        }

        public Dungeon SelectedDungeon
        {
            get { return selectedDungeon; }
            set
            {
                try
                {
                    #region Clear elements
                    Quests.Clear();
                    Rewards.Clear();
                    Tooltip = "";
                    RewardTooltip = "";
                    ItemIcon = null;
                    ItemBorders = null;
                    #endregion

                    selectedDungeon = value;
                    var questList = questService.SearchQuestById(selectedDungeon.Id);
                    foreach (var quest in questList)
                    {
                        quests.Add(quest);
                    }
                }
                catch (Exception ex)
                {
                    Toast.Show(ex.Message);
                }
            }
        }

        public Quest SelectedQuest
        {
            get { return selectedQuest; }
            set
            {
                try
                {
                    #region Clear elements
                    Rewards.Clear();
                    RewardTooltip = "";
                    ItemIcon = null;
                    ItemBorders = null;
                    #endregion
                    selectedQuest = value;
                    Tooltip = selectedQuest.Tooltip;
                    TapText = "Tap for details!";
                    SearchQuestRewards();
                }
                catch (Exception ex)
                {
                    Toast.Show(ex.Message);
                }
            }
        }

        private void SearchQuestRewards()
        {
            var items = itemService.SearchItemById(selectedQuest.Id);

            foreach (var item in items)
            {
                Rewards.Add(item);
            }
            if (Rewards.Count == 0)
            {
                Rewards.Add(new Item
                {
                    Name = "There are no item rewards in this Quest",
                    ItemColor = "#ffffff"
                });
            }
        }

        public Item SelectedReward
        {
            get { return selectedReward; }
            set
            {
                try
                {
                    selectedReward = value;

                    ItemIcon = null;
                    ItemBorders = null;

                    RewardTooltip = selectedReward.Tooltip;   
                    var icon = selectedReward.Icon;
                    ItemIcon = "https://wow.zamimg.com/images/wow/icons/large/" + icon + ".jpg";
                    ItemBorders = "iconBorders.png";
                }
                catch (Exception ex)
                {
                    Toast.Show(ex.Message);
                }
            }
        }

    }
}
