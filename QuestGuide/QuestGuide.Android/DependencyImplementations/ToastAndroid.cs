﻿using Android.Widget;
using QuestGuide.Dependencies;
using QuestGuide.Droid.DependencyImplementations;
using Xamarin.Forms;

[assembly: Dependency(typeof(ToastAndroid))]
namespace QuestGuide.Droid.DependencyImplementations
{
    public class ToastAndroid : IToast
    {
        private static Android.Widget.Toast toast;

        public void Show(string message)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (toast != null)
                    toast.Cancel();

                toast = Android.Widget.Toast.MakeText(Android.App.Application.Context, message, ToastLength.Short);
                toast.Show();
            });
        }
    }
}