﻿
using Android.Content;
using QuestGuide.Dependencies;
using QuestGuide.Droid.DependencyImplementations;
using Xamarin.Forms;

[assembly: Dependency(typeof(NativeSettingsAndroid))]
namespace QuestGuide.Droid.DependencyImplementations
{
    public class NativeSettingsAndroid : INativeSettings
    {

        private ISharedPreferences GetPreferences()
        {
            return Android.App.Application.Context.GetSharedPreferences("QuestGuideSettings", FileCreationMode.Private);
        }

        public string GetValue(string key)
        {
            return GetPreferences().GetString(key, null);
        }

        public void StoreValue(string key, string value)
        {
            var editor = GetPreferences().Edit();
            editor.PutString(key, value);
            editor.Commit();
        }

        public void RemoveValue(string key)
        {
            var editor = GetPreferences().Edit();
            editor.Remove(key);
            editor.Commit();
        }
    }
}